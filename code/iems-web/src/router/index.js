import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/systemSummary',
    children: [{
      path: 'systemSummary',
      name: 'SystemSummary',
      component: () => import('@/views/systemSummary/index'),
      meta: { title: '系统概况', icon: 'dashboard' }
    }]
  },

  {
    path: '/loadAnalysis',
    component: Layout,
    redirect: '/loadAnalysis/loadOverview',
    name: 'LoadAnalysis',
    meta: { title: '负荷分析', icon: 'loadAnalysis' },
    children: [
      {
        path: 'loadOverview',
        name: 'LoadOverview',
        component: () => import('@/views/loadAnalysis/loadOverview/index'),
        meta: { title: '负荷概况', icon: 'dashboard' }
      },
      {
        path: 'buildingEnergy',
        name: 'BuildingEnergy',
        component: () => import('@/views/loadAnalysis/buildingEnergy/index'),
        meta: { title: '建筑用能', icon: 'buildingEnergy' }
      }
      // {
      //   path: 'loadPrediction',
      //   name: 'LoadPrediction',
      //   component: () => import('@/views/loadAnalysis/loadPrediction/index'),
      //   meta: { title: '负荷预测', icon: 'loadPrediction' }
      // }
    ]
  },
  {
    path: '/energySupply',
    component: Layout,
    redirect: '/energySupply/energySupplyOverview',
    name: 'EnergySupply',
    meta: { title: '供能分析', icon: 'supply' },
    children: [
      {
        path: 'energySupplyOverview',
        name: 'EnergySupplyOverview',
        component: () => import('@/views/energySupply/energySupplyOverview/index'),
        meta: { title: '供能总览', icon: 'dashboard' }
      },

      {
        path: 'energyManage',
        name: 'EnergyManage',
        component: () => import('@/views/energySupply/energyManage/index'),
        meta: { title: '实时耗能', icon: 'supply' }
      },
      {
        path: 'productivityData',
        name: 'ProductivityData',
        component: () => import('@/views/energySupply/productivityData/index'),
        meta: { title: '产能数据', icon: 'productivityData' }
      },
      {
        path: 'operationPlan',
        name: 'OperationPlan',
        component: () => import('@/views/energySupply/operationPlan/index'),
        meta: { title: '运行计划', icon: 'supply-plan' }
      }
    ]
  },
  {
    path: '/mochaManage',
    component: Layout,
    redirect: '/mochaManage/deviceManage',
    name: 'MochaManage',
    meta: { title: '运维管理', icon: 'mochaManage' },
    children: [
      {
        path: 'deviceManage',
        name: 'DeviceManage',
        component: () => import('@/views/mochaManage/deviceManage/index'),
        meta: { title: '设备管理', icon: 'deviceManage' }
      },
      {
        path: 'operationLogs',
        name: 'OperationLogs',
        component: () => import('@/views/mochaManage/operationLogs/index'),
        meta: { title: '运行日志', icon: 'operationLogs' }
      },
      {
        path: 'alarmManage',
        name: 'AlarmManage',
        component: () => import('@/views/mochaManage/alarmManage/index'),
        meta: { title: '告警管理', icon: 'alarmManage' }
      },
      {
        path: 'reportManage',
        name: 'ReportManage',
        component: () => import('@/views/mochaManage/reportManage/index'),
        meta: { title: '报表管理', icon: 'report' },
        children: [
          {
            path: 'reportProductivity',
            name: 'ReportProductivity',
            component: () => import('@/views/mochaManage/reportManage/reportProductivity/index'),
            meta: { title: '产能报表', icon: 'report' }
          },
          {
            path: 'reportLoad',
            name: 'ReportLoad',
            component: () => import('@/views/mochaManage/reportManage/reportLoad/index'),
            meta: { title: '负荷报表', icon: 'report' }
          },
          {
            path: 'reportDevice',
            name: 'ReportDevice',
            component: () => import('@/views/mochaManage/reportManage/reportDevice/index'),
            meta: { title: '设备运行报表', icon: 'report' }
          },
          {
            path: 'reportEnergyConsumption',
            name: 'ReportEnergyConsumption',
            component: () => import('@/views/mochaManage/reportManage/reportEnergyConsumption/index'),
            meta: { title: '能耗报表', icon: 'report' }
          }
          // ,
          // {
          //   path: 'reportCost',
          //   name: 'ReportCost',
          //   component: () => import('@/views/mochaManage/reportManage/reportCost/index'),
          //   meta: { title: '能量运行费用报表', icon: 'table' }
          // }
        ]
      }
    ]
  },
  {
    path: '/cfgManage',
    component: Layout,
    redirect: '/cfgManage/cfgBasic',
    name: 'cfgManage',
    meta: { title: '配置管理', icon: 'cfgManage' },
    children: [
      {
        path: 'cfgBasic',
        name: 'CfgBasic',
        component: () => import('@/views/cfgManage/cfgBasic/index'),
        meta: { title: '基础配置', icon: 'cfgBasic' }
      },
      {
        path: 'cfgUser',
        name: 'CfgUser',
        component: () => import('@/views/cfgManage/cfgUser/index'),
        meta: { title: '用能用户管理', icon: 'userConfig' }
      },
      {
        path: 'cfgEnergySystem',
        name: 'CfgEnergySystem',
        component: () => import('@/views/cfgManage/cfgEnergySystem/index'),
        meta: { title: '系统配置', icon: 'cfgEnergySystem' }
      }

    ]
  },
  // {
  //   path: '/example',
  //   component: Layout,
  //   redirect: '/example/table',
  //   name: 'Example',
  //   meta: { title: 'Example', icon: 'example' },
  //   children: [
  //     {
  //       path: 'table',
  //       name: 'Table',
  //       component: () => import('@/views/table/index'),
  //       meta: { title: 'Table', icon: 'table' }
  //     },
  //     {
  //       path: 'tree',
  //       name: 'Tree',
  //       component: () => import('@/views/tree/index'),
  //       meta: { title: 'Tree', icon: 'tree' }
  //     }
  //   ]
  // },
  // {
  //   path: '/form',
  //   component: Layout,
  //   children: [
  //     {
  //       path: 'index',
  //       name: 'Form',
  //       component: () => import('@/views/form/index'),
  //       meta: { title: 'Form', icon: 'form' }
  //     }
  //   ]
  // },
  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
