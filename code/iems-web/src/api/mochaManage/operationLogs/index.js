import request from '@/utils/request'

export function list(params) {
  return request({
    url: '/vue-admin-template/operationLogs/list',
    method: 'get',
    params: params
  })
}
