import request from '@/utils/request'

export function list(params) {
  return request({
    url: '/vue-admin-template/deviceManage/list',
    method: 'get',
    params
  })
}

