import request from '@/utils/request'

export function list(params) {
  return request({
    url: '/vue-admin-template/reportManage/reportDevice/list',
    method: 'get',
    params: params
  })
}
