import request from '@/utils/request'

/**
 * 获取基础配置文件
 * @param {*} params
 */
export function getCfg(params) {
  return request({
    url: '/vue-admin-template/cfg/getCfg',
    method: 'get',
    params
  })
}

/**
 * 更新基础配置文件
 * @param {*} params
 */
export function updateCfg(params) {
  return request({
    url: '/vue-admin-template/cfg/updateCfg',
    method: 'get',
    params: params
  })
}

/**
 * 获取系统用户
 * @param {参数} params
 */
export function systemUserList(params) {
  return request({
    url: '/vue-admin-template/cfg/systemUserList',
    method: 'get',
    params: params
  })
}

/**
 * 获取能源系统
 * @param {参数} params
 */
export function energySystemList(params) {
  return request({
    url: '/vue-admin-template/cfg/energySystemList',
    method: 'get',
    params: params
  })
}

