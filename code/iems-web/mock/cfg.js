import Mock from 'mockjs'

const systemUserList = []
const energySystemList = []
const count = 21

for (let i = 1; i < count; i++) {
  systemUserList.push(Mock.mock({
    id: i,
    username: '用户'+i,
    userType: '写字楼',
    totalArea: '@integer(100000, 600000)',
    supplyArea: '@integer(50000, 100000)',
    supplyRate: '@integer(10, 80)' + '%',
    ecoRate: '@integer(10, 80)' + '%',
    heatLoad: '@integer(6000, 10000)',
    coldLoad: '@integer(6000, 10000)'
  }))
  energySystemList.push(Mock.mock({
    id: i,
    station: '3号站',
    systemName: '冰蓄冷系统',
    moduleName: '冰蓄冷1号机',
    deviceId: '3-a-101',
    heat: '@integer(10000, 60000)',
    heatElectricpower: '@integer(20, 60)',
    heatGaspower: '@integer(20, 60)',
    municipalHeat: '@integer(20, 60)',
    heatCop: '@float(3,8,1,1)',
    cold: '@integer(10000, 60000)',
    coldElectricpower: '@integer(20, 60)',
    coldGaspower: '@integer(10000, 60000)',
    coldCop: '@float(3,8,1,1)',
    energyType: '制冷'
  }))
}
// 基础配置表格
const cfgDataList = Mock.mock({
  'items|5': [{
    date: '2020-04-02',
    ePriceH: '@float(2,3,1,2)',
    ePriceL: '@float(0,1,1,2)',
    ePriceM: '@float(1,2,1,2)',
    qPrice: '@float(3,4,1,2)',
    waterPrice: '@float(4,5,1,1)',
    mhPrice: '@float(12,20,1,1)',
    user: 'admin'
  }]
})

export default [
  {
    url: '/vue-admin-template/cfg/getCfg',
    type: 'get',
    response: config => {
      const items = cfgDataList.items
      return {
        code: 20000,
        data: {
          total: items.length,
          items: items
        }
      }
    }
  },
  {
    url: '/vue-admin-template/cfg/systemUserList',
    type: 'get',
    response: config => {
      const { page = 1, limit = 10 } = config.query
      const mockList = systemUserList.filter(item => {
        return true
      })

      const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))

      return {
        code: 20000,
        data: {
          total: mockList.length,
          items: pageList
        }
      }
    }
  },
  {
    url: '/vue-admin-template/cfg/energySystemList',
    type: 'get',
    response: config => {
      const { page = 1, limit = 10 } = config.query
      const mockList = energySystemList.filter(item => {
        return true
      })

      const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))

      return {
        code: 20000,
        data: {
          total: mockList.length,
          items: pageList
        }
      }
    }
  },
  {
    url: '/vue-admin-template/cfg/buildList ',
    type: 'get',
    response: config => {
      const items = systemUserList
      return {
        code: 20000,
        data: {
          total: items.length,
          items: items
        }
      }
    }
  }
]
