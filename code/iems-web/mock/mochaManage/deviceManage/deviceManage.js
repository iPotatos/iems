import Mock from 'mockjs'

const data = Mock.mock({
  'items|15': [{
    id: '@integer(1, 100)',
    stationCode: '@natural',
    stationName: '@natural',
    deviceCode: '@natural',
    deviceName: '@natural',
    deviceType: Mock.mock('@sentence(2)'),
    lastRunTime: '@date(2020-03-dd)',
    deviceCurrentData: Mock.mock('@sentence(2)'),
    status: 'well'
  }]
})

export default [
  {
    url: '/vue-admin-template/deviceManage/list',
    type: 'get',
    response: config => {
      const allNum = data.items.length
      const items = data.items.slice((config.query.page - 1) * config.query.limit, (config.query.page - 1) * config.query.limit + config.query.limit)
      return {
        code: 20000,
        data: {
          total: allNum,
          items: items
        }
      }
    }
  }
]

