import Mock from 'mockjs'

const data = Mock.mock({
  'items|11': [{
    id: '@integer(1, 100)',
    date: '@date(2020-03-dd)',
    stationCode: '222',
    stationName: '3号能源站',
    allCost: Mock.mock('@natural(1, 99999)'),
    allWater: Mock.mock('@natural(1, 99999)'),
    waterCost: Mock.mock('@natural(1, 99999)'),
    allElectric: Mock.mock('@natural(1, 99999)'),
    allElectricCost: Mock.mock('@natural(1, 99999)'),
    allPeakElectric: Mock.mock('@natural(1, 99999)'),
    peakElectricCost: Mock.mock('@natural(1, 99999)'),
    allValleyElectric: Mock.mock('@natural(1, 99999)'),
    valleyElectricCost: Mock.mock('@natural(1, 99999)'),
    averageElectric: Mock.mock('@natural(1, 99999)'),
    averageElectricCost: Mock.mock('@natural(1, 99999)'),
    allGas: Mock.mock('@natural(1, 99999)'),
    gasCost: Mock.mock('@natural(1, 99999)'),
    cityGovernmentHeat: Mock.mock('@natural(1, 99999)'),
    cityGovernmentHeatCost: Mock.mock('@natural(1, 99999)')
  }]
})

export default [
  {
    url: '/vue-admin-template/reportManage/reportEnergyConsumption/list',
    type: 'get',
    response: config => {
      const allNum = data.items.length
      const items = data.items.slice((config.query.page - 1) * config.query.limit, (config.query.page - 1) * config.query.limit + config.query.limit)
      return {
        code: 20000,
        data: {
          total: allNum,
          items: items
        }
      }
    }
  }
]

