import Mock from 'mockjs'

const data = Mock.mock({
  'items|12': [{
    id: '@integer(1, 100)',
    date: '@date(2020-03-dd)',
    user: '用户1',
    dayAllLoad: '@integer(1000, 10000)',
    oneHour: '@integer(1, 100)',
    twoHour: '@integer(1, 101)',
    threeHour: '@integer(1, 102)',
    fourHour: '@integer(1, 103)',
    fiveHour: '@integer(1, 104)',
    sixHour: '@integer(1, 105)',
    sevenHour: '@integer(1, 106)',
    eightHour: '@integer(1, 107)',
    nineHour: '@integer(1, 108)',
    tenHour: '@integer(1, 109)',
    elevenHour: '@integer(1, 110)',
    twelveHour: '@integer(1, 111)',
    thirteenHour: '@integer(1, 112)',
    fourteenHour: '@integer(1, 113)',
    fifteenHour: '@integer(1, 114)',
    sixteenHour: '@integer(1, 115)',
    seventeenHour: '@integer(1, 116)',
    eighteenHour: '@integer(1, 117)',
    nineteenHour: '@integer(1, 118)',
    twentyHour: '@integer(1, 119)',
    twentyOneHour: '@integer(1, 120)',
    twentyTwoHour: '@integer(1, 121)',
    twentyThreeHour: '@integer(1, 122)',
    twentyFourHour: '@integer(1, 123)'
  }]
})

export default [
  {
    url: '/vue-admin-template/reportManage/reportLoad/list',
    type: 'get',
    response: config => {
      const allNum = data.items.length
      const items = data.items.slice((config.query.page - 1) * config.query.limit, (config.query.page - 1) * config.query.limit + config.query.limit)
      return {
        code: 20000,
        data: {
          total: allNum,
          items: items
        }
      }
    }
  }
]

