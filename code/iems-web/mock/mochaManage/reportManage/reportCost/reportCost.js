import Mock from 'mockjs'

const data = Mock.mock({
  'items|11': [{
    id: '@integer(1, 100)',
    deviceCode: '@natural',
    operation: Mock.mock('@sentence(4)'),
    operationTime: '@date(2020-03-dd)',
  }]
})

export default [
  {
    url: '/vue-admin-template/reportManage/reportCost/list',
    type: 'get',
    response: config => {
      const allNum = data.items.length
      const items = data.items.slice((config.query.page - 1) * config.query.limit, (config.query.page - 1) * config.query.limit + config.query.limit)
      return {
        code: 20000,
        data: {
          total: allNum,
          items: items
        }
      }
    }
  }
]

