import Mock from 'mockjs'

const data = Mock.mock({
  'items|11': [{
    id: '@integer(1, 100)',
    date: '@date(2020-03-dd)',
    stationCode: '1',
    stationName: '1号能源站',
    system: '2号系统',
    plantUnit: '3号机组',
    capacity: '@integer(1, 100)',
    dayAllRunTime: '@integer(1, 360)'
  }]
})

export default [
  {
    url: '/vue-admin-template/reportManage/reportProductivity/list',
    type: 'get',
    response: config => {
      const allNum = data.items.length
      const items = data.items.slice((config.query.page - 1) * config.query.limit, (config.query.page - 1) * config.query.limit + config.query.limit)
      return {
        code: 20000,
        data: {
          total: allNum,
          items: items
        }
      }
    }
  }
]

