import Mock from 'mockjs'

const data = Mock.mock({
  'items|11': [{
    // id: '@integer(1, 100)',
    // deviceCode: 'NY-001' + '@natural',
    // operation: '修改NY-001' + '@natural',
    // operationTime: '@datetime'
    id: '@integer(1, 100)',
    stationCode: '1',
    stationName: '1号能源站',
    deviceCode: 'NY-001',
    deviceName: '冰蓄冷1号机',
    deviceType: '系统模块',
    lastRunTime: '@date(2020-03-dd)',
    deviceCurrentData: Mock.mock('@sentence(2)'),
    operationRecord: '打开',
    operationTime: '@date(2020-03-dd)'
  }]
})

export default [
  {
    url: '/vue-admin-template/operationLogs/list',
    type: 'get',
    response: config => {
      const allNum = data.items.length
      const items = data.items.slice((config.query.page - 1) * config.query.limit, (config.query.page - 1) * config.query.limit + config.query.limit)
      return {
        code: 20000,
        data: {
          total: allNum,
          items: items
        }
      }
    }
  }
]

