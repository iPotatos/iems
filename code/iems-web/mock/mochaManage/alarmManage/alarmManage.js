import Mock from 'mockjs'

const data = Mock.mock({
  'items|11': [{
    id: '@integer(1, 100)',
    faultType: '500',
    faultContent: Mock.mock('@sentence(4)'),
    status: '已修复',
    occurTime: '@date(2020-03-dd)'
  }]
})

export default [
  {
    url: '/vue-admin-template/alarmManage/list',
    type: 'get',
    response: config => {
      const allNum = data.items.length
      const items = data.items.slice((config.query.page - 1) * config.query.limit, (config.query.page - 1) * config.query.limit + config.query.limit)
      return {
        code: 20000,
        data: {
          total: allNum,
          items: items
        }
      }
    }
  }
]

